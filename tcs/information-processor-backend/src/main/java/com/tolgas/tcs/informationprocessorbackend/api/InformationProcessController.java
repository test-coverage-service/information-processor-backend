package com.tolgas.tcs.informationprocessorbackend.api;

import com.tolgas.tcs.informationprocessorbackend.model.document.InformationActionDocument;
import com.tolgas.tcs.informationprocessorbackend.model.document.InformationProcessDocument;
import com.tolgas.tcs.informationprocessorbackend.repository.InformationProcessRepository;
import com.tolgas.tcs.informationprocessorbackend.service.InformationProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.tolgas.tcs.informationprocessorbackend.model.document.InformationActionDocument.ActionType.REST;

@RestController
public class InformationProcessController {

    @Autowired
    InformationProcessRepository informationProcessRepository;

    @Autowired
    InformationProcessService informationProcessService;

    @PostMapping("/api/v1/information-process")
    public InformationProcessDocument create(@RequestBody InformationProcessDocument informationProcessDocument) {
        InformationProcessDocument informationProcessDocument1 = new InformationProcessDocument();
        informationProcessDocument1.setId(1L);
        informationProcessDocument1.setActions(
                List.of(
                        new InformationActionDocument(1L, 1L, REST, InformationActionDocument.ProcessResult.SUCCESS),
                        new InformationActionDocument(2L, 2L, REST, InformationActionDocument.ProcessResult.SUCCESS),
                        new InformationActionDocument(3L, 3L, REST, InformationActionDocument.ProcessResult.FAILED)
                )
        );

        return informationProcessRepository.save(informationProcessDocument);
    }

    @GetMapping("/api/v1/information-process/{id}")
    public InformationProcessDocument get(@PathVariable Long id) {
        return informationProcessRepository.findById(id).get();
    }

    @DeleteMapping("/api/v1/information-process/{id}")
    public void delete(@PathVariable Long id) {
        informationProcessRepository.deleteById(id);
    }

    @PutMapping("/api/v1/information-process/{id}")
    public InformationProcessDocument update(@PathVariable Long id,
                                             @RequestBody InformationProcessDocument informationProcessDocument) {
        informationProcessDocument.setId(id);
        return informationProcessRepository.save(informationProcessDocument);
    }

    @PostMapping("/api/v1/information-process/{id}/process")
    public void process(@PathVariable Long id) {
        informationProcessService.process(id);
    }
}
