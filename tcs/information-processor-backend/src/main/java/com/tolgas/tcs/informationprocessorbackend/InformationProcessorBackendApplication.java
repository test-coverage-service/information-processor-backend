package com.tolgas.tcs.informationprocessorbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformationProcessorBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InformationProcessorBackendApplication.class, args);
    }

}
