package com.tolgas.tcs.informationprocessorbackend.repository;

import com.tolgas.tcs.informationprocessorbackend.model.document.InformationProcessDocument;
import org.springframework.data.repository.CrudRepository;

public interface InformationProcessRepository extends CrudRepository<InformationProcessDocument, Long> {
}
